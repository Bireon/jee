package fr.epita.hivers;

import fr.epita.hivers.utils.Provider;
import fr.epita.hivers.utils.Rest;
import fr.epita.hivers.utils.Scope;

import java.util.*;

public class DefaultScope implements Scope {

    private final Map<Class<?>, Provider<?>> providerMap = new HashMap<>();
    private final List<Rest> rests = new ArrayList<>();

    public void register(final Provider<?> provider) {
        providerMap.put(provider.getProviderClass(), provider);
    }

    @SuppressWarnings("unchecked")
    public <REQ_T> Optional<? extends REQ_T> instanceOf(final Class<REQ_T> boundClass) {
        var provider = providerMap.get(boundClass);
        return provider != null
                ? Optional.of((REQ_T) provider.getValue())
                : Optional.empty();
    }

    public void addRest(Rest rest) {
        rests.add(rest);
    }

    public <REQ_T> Rest getRest(Class<REQ_T> bounClass) {

        for (int i = rests.size() - 1; i >= 0; i--) {
            var rest = rests.get(i);

            if (rest.getType() == bounClass) {
                return rest;
            }
        }

        return null;
    }
}

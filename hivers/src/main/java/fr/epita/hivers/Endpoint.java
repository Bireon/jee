package fr.epita.hivers;

import fr.epita.hivers.utils.Context;

import java.util.function.Consumer;

public record Endpoint(RestHivers.Method method, String path, Consumer<Context> consumer) {
}

package fr.epita.hivers;

import fr.epita.hivers.utils.Provider;
import fr.epita.hivers.utils.Rest;
import fr.epita.hivers.utils.Scope;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Hivers {

    private final List<Scope> scopes = new ArrayList<>();

    public Hivers() {
        scopes.add(new DefaultScope());
    }

    public void pop() {
        if (scopes.size() > 1) {
            scopes.remove(scopes.size() - 1);
        } else {
            throw new RuntimeException("Exception there is nothing to remove");
        }
    }

    public void push(Scope scope) {
        scopes.add(scope);
    }

    public <PROVIDED_T> Provider<PROVIDED_T> provider(final Provider<PROVIDED_T> provider) {
        var topOfStack = scopes.get(scopes.size() - 1);
        topOfStack.register(provider);
        return provider;
    }

    public <REQ_T> Optional<? extends REQ_T> instanceOf(Class<REQ_T> boundClass) {
        for (int i = scopes.size() - 1; i >= 0; i--) {
            var scope = scopes.get(i);

            var optional = scope.instanceOf(boundClass);
            if (optional.isPresent()) {
                return optional;
            }
        }

        return Optional.empty();
    }

    public void register(Rest rest) {
        var topOfStack = scopes.get(scopes.size() - 1);
        topOfStack.addRest(rest);
    }

    public <REQ_T> Rest extension(Class<REQ_T> bounClass) {

        var topOfStack = scopes.get(scopes.size() - 1);

        var rest = topOfStack.getRest(bounClass);

        if (rest == null) {
            throw new RuntimeException("Error couldn't get any RestAPI because there was none that have been added.");
        }

        return rest;
    }
}

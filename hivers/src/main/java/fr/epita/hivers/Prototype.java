package fr.epita.hivers;

import fr.epita.hivers.utils.AbstractProvider;
import java.util.function.Supplier;


public class Prototype<PROVIDED_T, VALUE_T extends PROVIDED_T> extends AbstractProvider<PROVIDED_T> {

    private final Supplier<? extends PROVIDED_T> valueSupplier;

    public Prototype(Class<PROVIDED_T> boundClass, Supplier<? extends PROVIDED_T> valueSupplier) {
        super(boundClass);
        this.valueSupplier = valueSupplier;
    }

    @Override
    public Class<PROVIDED_T> getProviderClass() {
        return boundClass;
    }

    @Override
    public PROVIDED_T getValue() {
        var value = valueSupplier.get();

        return getProxy(value);
    }
}

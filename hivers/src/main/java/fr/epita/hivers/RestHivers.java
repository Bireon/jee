package fr.epita.hivers;

import static spark.Spark.*;

import fr.epita.hivers.utils.Context;
import fr.epita.hivers.utils.Rest;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class RestHivers implements Rest {

    private final List<Endpoint> endpoints = new ArrayList<>();

    @Override
    public void start() {
        for (Endpoint endpoint : endpoints) {
            if (endpoint.method() == Method.GET) {
                get(endpoint.path(), ((request, response) -> {
                    endpoint.consumer().accept(new Context(response));
                    return response.body();
                }));
            }

            if (endpoint.method() == Method.DELETE) {
                delete(endpoint.path(), ((request, response) -> {
                    endpoint.consumer().accept(new Context(response));
                    return response.body();
                }));
            }
        }
    }

    @Override
    public void shutdown() {
        stop();
    }

    @Override
    public Rest register(final Method method, final String path, final Consumer<Context> consumer) {
        endpoints.add(new Endpoint(method, path, consumer));

        return this;
    }

    @Override
    public Class<? extends Rest> getType() {
        return RestHivers.class;
    }


    public enum Method {
        GET,
        DELETE
    }
}

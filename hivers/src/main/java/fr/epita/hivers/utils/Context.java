package fr.epita.hivers.utils;

import spark.Response;

public class Context {

    private final Response response;

    public Context(Response response) {
        this.response = response;
    }

    public void response(int status) {
        response.status(status);
    }

    public void response(int status, String responseData) {
        response.status(status);
        response.body(responseData);
    }
}

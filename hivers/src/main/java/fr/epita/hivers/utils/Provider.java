package fr.epita.hivers.utils;

import fr.epita.hivers.ProxyDefinition;

public interface Provider<PROVIDED_T> {

    Class<PROVIDED_T> getProviderClass();

    PROVIDED_T getValue();

    Provider<PROVIDED_T> withProxies(final ProxyDefinition... definitions);
}

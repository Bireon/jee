package fr.epita.hivers.utils;

import fr.epita.hivers.RestHivers;

import java.util.function.Consumer;

public interface Rest {

    void start();

    void shutdown();

    Rest register(RestHivers.Method method, String path, Consumer<Context> consumer);

    Class<? extends Rest> getType();
}

package fr.epita.hivers.utils;

import java.util.Optional;

public interface Scope {
    void register(final Provider<?> provider);

    <REQ_T> Optional<? extends REQ_T> instanceOf(final Class<REQ_T> boundClass);

    void addRest(final Rest rest);

    <REQ_T> Rest getRest(Class<REQ_T> bounClass);
}

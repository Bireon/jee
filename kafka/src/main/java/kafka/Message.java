package kafka;

public class Message {

    public int offset;
    public String content;

    public Message(int offset, String content) {
        this.offset = offset;
        this.content = content;
    }
}

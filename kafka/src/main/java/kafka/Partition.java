package kafka;

import java.util.ArrayList;
import java.util.List;

public class Partition {

    private List<Message> messages = new ArrayList<>();

    public boolean write(String messageContent) {
       return messages.add(new Message(messages.size(), messageContent));
    }
}

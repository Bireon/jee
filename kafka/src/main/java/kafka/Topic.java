package kafka;

import java.util.ArrayList;
import java.util.List;

public class Topic {

    public int id;
    public String name;
    public List<Partition> partitionList = new ArrayList<>();

    public Topic(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

package fr.epita.tfidf;

import fr.epita.tfidf.tools.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class Query {

    private Tokenizer tokenizer;
    private final WordDictionary wordDictionary = new WordDictionary(new HashMap<>(), 0);

    public Query(String stopWordPath, String synonymsPath) throws IOException {
        tokenizer = new Tokenizer(stopWordPath, synonymsPath);
    }

    private boolean validDocument(Document document) {
        for (List<Document> documents : wordDictionary.getDictionary().values()) {
            if (documents.contains(document)) {
                return false;
            }
        }

        return true;
    }

    public void tmpAdd(String url, String body) {
        List<TokenItem> tokenItems = tokenizer.createTokenVector(body);

        Document document = new Document(url, new TokenVector(tokenItems));

        if (!validDocument(document)) {
            return;
        }

        for (TokenItem tokenItem : tokenItems) {
            String word = tokenItem.getName();

            if (wordDictionary.getDictionary().containsKey(word)) {
                wordDictionary.getDictionary().get(word).add(document);
            } else {
                List<Document> documents = new ArrayList<>();
                documents.add(document);

                wordDictionary.getDictionary().put(word, documents);
            }
        }

        wordDictionary.setSize(wordDictionary.getSize() + 1);
    }

    public void add(String url) {
        var documentText = HTMLParser.fetchDocumentText(url);

        if (documentText == null) {
            return;
        }

        List<TokenItem> tokenItems = tokenizer.createTokenVector(documentText);

        Document document = new Document(url, new TokenVector(tokenItems));

        if (!validDocument(document)) {
            return;
        }

        for (TokenItem tokenItem : tokenItems) {
            String word = tokenItem.getName();

            if (wordDictionary.getDictionary().containsKey(word)) {
                wordDictionary.getDictionary().get(word).add(document);
            } else {
                List<Document> documents = new ArrayList<>();
                documents.add(document);

                wordDictionary.getDictionary().put(word, documents);
            }
        }

        wordDictionary.setSize(wordDictionary.getSize() + 1);
    }

    private Optional<Double> findToken(String word, List<TokenItem> list) {
        for (TokenItem tokenItem : list) {
            if (tokenItem.getName().equals(word)) {
                return Optional.of(tokenItem.getFrequency());
            }
        }

        return Optional.empty();
    }

    private double cosineCalcul(TokenVector query, TokenVector document, double idf) {
        List<Double> queryVector = new ArrayList<>();
        List<Double> documentVector = new ArrayList<>();

        for (TokenItem tokenItem : query.tokenItems()) {
            var finder = findToken(tokenItem.getName(), document.tokenItems());

            if (finder.isPresent()) {
                queryVector.add(tokenItem.getFrequency() * idf);
                documentVector.add(finder.get());
            }
        }

        return MathTools.computeSimiliarity(queryVector, documentVector);
    }

    public List<Document> search(String request) {

        TokenVector query = new TokenVector(tokenizer.createTokenVector(request));

        List<Double> cosineSimilarities = new ArrayList<>();
        List<Document> result = new ArrayList<>();

        for (TokenItem tokenItem : query.tokenItems()) {
            for (Document document : wordDictionary.getDictionary().get(tokenItem.getName())) {
                double cosine = cosineCalcul(query,
                        document.tokenVector(),
                        MathTools.idf(tokenItem.getName(), wordDictionary));

                int i = 0;

                if (cosineSimilarities.size() == 0) {
                    cosineSimilarities.add(cosine);
                    result.add(document);
                } else {
                    while (i < cosineSimilarities.size() && cosine > cosineSimilarities.get(i)) {
                        i += 1;
                    }

                    cosineSimilarities.add(i, cosine);
                    result.add(i, document);
                }
            }
        }

        return result;
    }
}

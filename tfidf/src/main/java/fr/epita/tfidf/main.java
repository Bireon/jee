package fr.epita.tfidf;

import fr.epita.tfidf.tools.Document;

import java.io.IOException;
import java.util.List;

public class main {

    public static void main(String[] args) throws IOException {
        Query query = new Query("src/main/resources/englishStopWords.json",
                "src/main/resources/englishSynonymList.jsonl");

        String firstAddString = "The blue rabbit is fishing in a blue river.";
        String secondAddString = "The blue whale was a very big blue whale in the blue ocean.";
        String thirdAddString = "I'm blue.";

        query.tmpAdd("first", firstAddString);
        query.tmpAdd("second", secondAddString);
        query.tmpAdd("third", thirdAddString);

        List<Document> result = query.search("blue");

        for (Document document : result) {
            System.out.println(document.name());
        }
    }

}

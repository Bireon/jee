package fr.epita.tfidf.tools;

public record Document(String name, TokenVector tokenVector) {
}

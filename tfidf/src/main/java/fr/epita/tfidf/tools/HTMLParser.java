package fr.epita.tfidf.tools;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class HTMLParser {

    public static String fetchDocumentText(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            return doc.body().text();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

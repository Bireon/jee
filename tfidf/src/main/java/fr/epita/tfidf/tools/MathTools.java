package fr.epita.tfidf.tools;

import java.util.List;

public class MathTools {

    public static double idf(String word, WordDictionary wordDictionary) {
        double fraction =  (double) wordDictionary.getSize() / (1 + wordDictionary.getDictionary().get(word).size());

        return Math.log(fraction);
    }

    private  static double dotProduct(List<Double> firstVector, List<Double> secondVector) {
        double result = 0;

        for (int i = 0; i < firstVector.size(); i++) {
            result += firstVector.get(i) * secondVector.get(i);
        }

        return result;
    }

    private static double euclidianDst(List<Double> firstVector, List<Double> secondVector) {
        double sum = 0;

        for (int i = 0; i < firstVector.size(); i++) {
            sum += (firstVector.get(i) - secondVector.get(i)) * (firstVector.get(i) - secondVector.get(i));
        }

        return Math.sqrt(sum);
    }

    public static double computeSimiliarity(List<Double> firstVector, List<Double> secondVector) {
        double productResult = dotProduct(firstVector, secondVector);
        double distanceResult = euclidianDst(firstVector, secondVector);

        return productResult / distanceResult;
    }
}

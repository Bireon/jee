package fr.epita.tfidf.tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Stemmer {

    public static String stem(String s) {
        if (s.length() <= 3) return s;

        var step1 = step1(s);
        var step2 = step2(step1);
        var step3 = step3(step2);
        var step4 = step4(step3);
        var step5 = step5(step4);

        return step5;
    }

    public static String step1(String s) {
        var strLastIndex = s.length();

        // a
        if (s.endsWith("sses")) return s.substring(0, strLastIndex - 2);
        if (s.endsWith("ies"))return s.substring(0, strLastIndex - 2);
        if (s.endsWith("s")) return s.substring(0, strLastIndex - 1);

        // b
        if (s.endsWith("eed") && countM(s) > 0) return s.substring(0, strLastIndex - 1);
        if (s.endsWith("ed")) return s.substring(0, strLastIndex - 2);
        if (s.endsWith("ing")) return s.substring(0, strLastIndex - 3);

        // c
        if (s.endsWith("y")) return s.substring(0, strLastIndex - 1);

        return s;
    }

    public static String step2(String s) {
        Map<String, String> dictionary = new HashMap<String, String>() {{
            put("ational", "ate");
            put("tional", "tion");
            put("enci", "ence");
            put("anci", "ance");
            put("izer", "ize");
            put("bli", "ble");
            put("alli", "al");
            put("entli", "ent");
            put("eli", "e");
            put("ousli", "ous");
            put("ization", "ize");
            put("ation", "ate");
            put("ator", "ate");
            put("alism", "al");
            put("iveness", "ive");
            put("fulness", "ful");
            put("ousness", "ous");
            put("aliti", "al");
            put("iviti", "ive");
            put("biliti", "ble");
            put("logi", "log");
        }};

        for (Map.Entry<String, String> entry : dictionary.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (countM(s) > 0 && s.endsWith(key)) {
                return s.substring(0, s.length() - key.length()) + value;
            }
        }

        return s;
    }

    public static String step3(String s) {
        Map<String, String> dictionary = new HashMap<String, String>() {{
            put("icate", "ic");
            put("ative", "");
            put("alize", "al");
            put("iciti", "ic");
            put("ical", "ic");
            put("ful", "");
            put("ness", "");
        }};

        for (Map.Entry<String, String> entry : dictionary.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (countM(s) > 0 && s.endsWith(key)) {
                return s.substring(0, s.length() - key.length()) + value;
            }
        }

        return s;
    }

    private static String step4(String s) {
        String[] suffixes = new String[]{
                "al",
                "ance",
                "ence",
                "er",
                "ic",
                "able",
                "ible",
                "ant",
                "ement",
                "ment",
                "ent",
                "ion",
                "ou",
                "ism",
                "ate",
                "iti",
                "ous",
                "ive",
                "ize",
        };

        for (String suffix : suffixes) {
            if (countM(s) > 1 && s.endsWith(suffix)) {
                return s.substring(0, s.length() - suffix.length());
            }
        }

        return s;
    }

    private static String step5(String s) {
        if (countM(s) > 1 && s.endsWith("e")) return s.substring(0, s.length() - 1);
        return s;
    }

    private static boolean isVowel(Optional<Character> prev, Character curr) {
        if (prev.isPresent() && curr == 'y' && !isVowel(Optional.empty(), prev.get())) return true;
        return curr == 'a' || curr == 'e' || curr =='i' || curr == 'o' || curr == 'u';
    }

    // troubles => "ccvcccvc" => "C(VC)^2"
    private static String getPorterRepresentation(String input) {
        StringBuilder res = new StringBuilder();

        var isCurrentVowel = isVowel(Optional.empty(), input.charAt(0));
        for (int i = 0; i < input.length(); i++) {
            Optional<Character> prevChar = i == 0 ? Optional.empty() : Optional.of(input.charAt(i - 1));
            // check if current character is same type as the previous char
            if (isCurrentVowel != isVowel(prevChar, input.charAt(i))) {
                res.append(isCurrentVowel ? 'V' : 'C');
                isCurrentVowel = !isCurrentVowel;
            }
        }
        res.append(isCurrentVowel ? 'V' : 'C');
        return res.toString();
    }

    private static Integer countM(String input)  {
        var porterString = getPorterRepresentation(input);
        if (porterString.length() < 2) return 0;
        if (porterString.charAt(0) == 'c') return (porterString.length() - 1) / 2;
        return porterString.length() / 2;
    }
}
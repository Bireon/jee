package fr.epita.tfidf.tools;

import java.util.List;

public record Stopper(List<String> stopWords) {
}

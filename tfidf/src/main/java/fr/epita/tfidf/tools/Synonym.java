package fr.epita.tfidf.tools;

import java.util.List;

public record Synonym(String word, String key, String pos, List<String> synonyms) {
}

package fr.epita.tfidf.tools;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class TokenItem {
    private String name;
    private double frequency;
    private final List<Integer> position;
}

package fr.epita.tfidf.tools;

import java.util.List;

public record TokenVector(List<TokenItem> tokenItems) {
}

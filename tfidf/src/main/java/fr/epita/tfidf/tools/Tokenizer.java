package fr.epita.tfidf.tools;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Tokenizer {
    private final List<String> stopWords;
    private final List<Synonym> synonyms = new ArrayList<>();

    public Tokenizer(String stopWordPath, String synonymsPath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        Stopper stopWordsGetter = objectMapper.readValue(
                new File(stopWordPath), Stopper.class);
        stopWords = stopWordsGetter.stopWords();

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(synonymsPath));
            String line = reader.readLine();
            while (line != null) {

                Synonym synonym = objectMapper.readValue(line, Synonym.class);
                synonyms.add(synonym);

                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Optional<String> SynonymFinder(String word) {
        List<Synonym> finder = synonyms.stream().filter(elm -> {
            return elm.word().equals(word);
        }).collect(Collectors.toList());

        if (finder.size() == 0 || finder.get(0).synonyms().size() == 0) {
            return Optional.empty();
        }

        return Optional.of(finder.get(0).synonyms().get(0));
    }

    private List<TokenItem> getVector(List<String> tokens) {
        List<TokenItem> result = new ArrayList<>();

        for (int i = 0; i < tokens.size(); i++) {
            String word = tokens.get(i);
            TokenItem findTokenItem = result.stream()
                    .filter(elm -> elm.getName().equals(word))
                    .findAny()
                    .orElse(null);

            if (findTokenItem == null) {
                double frequency = 1 / (double) tokens.size();

                List<Integer> position = new ArrayList<>();
                position.add(i);

                result.add(new TokenItem(word, frequency, position));
            } else {
                findTokenItem.getPosition().add(i);
                findTokenItem.setFrequency((double) findTokenItem.getPosition().size() / tokens.size());
            }
        }

        return result;
    }

    public List<TokenItem> createTokenVector(String document) {
        String[] preToken = document.replaceAll("\\p{Punct}", " ").split(" ");

        List<String> tokens = Arrays.stream(preToken).map(String::toLowerCase).collect(Collectors.toList());

        tokens.removeAll(stopWords);
        tokens = tokens.stream().map(Stemmer::stem).collect(Collectors.toList());

        tokens = tokens.stream().map(token -> {
            Optional<String> synonym = SynonymFinder(token);
            return synonym.orElse(token);
        }).toList();

        return getVector(tokens);
    }
}
package fr.epita.tfidf.tools;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Getter
@Setter
public class WordDictionary {
    private final Map<String, List<Document>> dictionary;
    private int size;
}
